-- 시퀀스 생성
DROP SEQUENCE SEQ_ARTISTCATEGORY_CODE;
DROP SEQUENCE SEQ_PLAY_ORDER;

CREATE SEQUENCE SEQ_ARTISTCATEGORY_CODE;
CREATE SEQUENCE SEQ_PLAY_ORDER;

ALTER SEQUENCE SEQ_ARTISTCATEGORY_CODE NOCACHE;
ALTER SEQUENCE SEQ_PLAY_ORDER NOCACHE;

-- 테이블 생성
DROP TABLE tbl_artistcategory CASCADE CONSTRAINTS;
DROP TABLE tbl_playlist CASCADE CONSTRAINTS;

-- artistcategory 테이블 생성
CREATE TABLE tbl_artistcategory
(
    artistcategory_code    NUMBER NOT NULL,
    artistcategory_name    VARCHAR2(30) NOT NULL
);

COMMENT ON COLUMN tbl_artistcategory.category_code IS '가수카테고리코드';
COMMENT ON COLUMN tbl_artistcategory.category_name IS '가수카테고리코드명';
COMMENT ON TABLE tbl_artistcategory IS '가수카테고리';

CREATE UNIQUE INDEX index_artistcategory_code ON tbl_artistcategory
( artistcategory_code );

ALTER TABLE tbl_artistcategory
 ADD CONSTRAINT pk_artistcategory_code PRIMARY KEY ( artistcategory_code )
 USING INDEX index_artistcategory_code;
 
-- playlist 테이블 생성
 CREATE TABLE tbl_playlist
(
    play_order    NUMBER NOT NULL,
    artist_name    VARCHAR2(30) NOT NULL,
    song_name    VARCHAR2(100) NOT NULL,
    artistcategory_code    NUMBER NOT NULL,
    like_status    CHAR(1) NOT NULL
);

COMMENT ON COLUMN tbl_playlist.play_order IS '재생순서';
COMMENT ON COLUMN tbl_playlist.artist_name IS '가수명';
COMMENT ON COLUMN tbl_playlist.song_name IS '노래제목';
COMMENT ON COLUMN tbl_playlist.artistcategory_code IS '가수카테고리코드';
COMMENT ON COLUMN tbl_playlist.like_status IS '좋아요상태';
COMMENT ON TABLE tbl_playlist IS '플레이리스트';

CREATE UNIQUE INDEX index_play_order ON tbl_playlist
( play_order );

ALTER TABLE tbl_playlist
 ADD CONSTRAINT pk_play_order PRIMARY KEY (play_order )
 USING INDEX index_play_order;

ALTER TABLE tbl_playlist
 ADD CONSTRAINT fk_artistcategory_code FOREIGN KEY ( artistcategory_code )
 REFERENCES tbl_artistcategory ( artistcategory_code );
 
--카테고리 코드 입력
INSERT INTO TBL_ARTISTCATEGORY VALUES (SEQ_ARTISTCATEGORY_CODE.NEXTVAL, '여성솔로');
INSERT INTO TBL_ARTISTCATEGORY VALUES (SEQ_ARTISTCATEGORY_CODE.NEXTVAL, '남성솔로');
INSERT INTO TBL_ARTISTCATEGORY VALUES (SEQ_ARTISTCATEGORY_CODE.NEXTVAL, '여성그룹');
INSERT INTO TBL_ARTISTCATEGORY VALUES (SEQ_ARTISTCATEGORY_CODE.NEXTVAL, '남성그룹');
INSERT INTO TBL_ARTISTCATEGORY VALUES (SEQ_ARTISTCATEGORY_CODE.NEXTVAL, '기타');

--플레이리스트 입력
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '윤하', '오르트구름', 1, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '태연', '불티', 1, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '시우민', 'BRAND NEW', 2, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '백현', 'BAMBI', 2, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '뉴진스', 'Ditto', 3, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '르세라핌', 'Blue Flame', 3, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '엑소', 'LOVE SHOT', 4, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '엑소', '첫눈', 4, 'Y');
INSERT INTO TBL_PLAYLIST VALUES (SEQ_PLAY_ORDER.NEXTVAL, '이영지', 'NOT SORRY', 5, 'Y');

COMMIT;
