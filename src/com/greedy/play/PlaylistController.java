package com.greedy.play;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.greedy.common.PlaylistDTO;

public class PlaylistController {
	
	private final PlaylistService playlistService;
	private final PrintResult printResult;

	
	public PlaylistController () {
		playlistService = new PlaylistService();
		printResult = new PrintResult();
		
	}

	public void selectAllPlaylist() {
		
		
		List<PlaylistDTO> plist = playlistService.selectAllPlaylist();
		
		if(plist != null) {
			printResult.printPlist(plist);
		} else {
			printResult.printErrorMessage("selectList");
		}
		
	
	}

	public void selectPlaylistByArtist(Map<String, String> parameter) {
		
		String name = parameter.get("name");
		
		List<PlaylistDTO> playlist = playlistService.selectPlaylistByArtist(name);
		
		if(playlist != null) {
			printResult.printPlaylist(playlist);
		} else {
			printResult.printErrorMessage("selectname");
		}
		
	}
	

	public void randomplay () {
	
	PlaylistService playlistService = new PlaylistService();
		
	playlistService.selectRandomPlaylist(createRandomPlaylist());;
	

	}
		
	
	
	private List<Integer> createRandomPlaylist() {
		
		Set<Integer> set = new HashSet<>();
		while(set.size() < 5  ) {
			int temp = ((int) (Math.random()*9)) + 1;
			set.add(temp);
		}
		
		List<Integer> random = new ArrayList<>(set);
		return random;
		
	}

	public void insertPlaylist(Map<String, String> parameter) {
		
		PlaylistDTO playlist = new PlaylistDTO();
		playlist.setArtistName(parameter.get("artistName"));
		playlist.setSongName(parameter.get("songName"));
		playlist.setArtistcategoryCode(Integer.parseInt(parameter.get("artistcategoryCode")));
		
		if(playlistService.registPlaylist(playlist)) {
			printResult.printSuccessMessage("insert");
		} else {
			printResult.printErrorMessage("insert");
		}
 	}
	

	public void modifyPlaylist(Map<String, String> parameter) {
		PlaylistDTO playlist = new PlaylistDTO();
		playlist.setPlayOrder(Integer.parseInt(parameter.get("order")));
		playlist.setArtistName(parameter.get("artistName"));
		playlist.setSongName(parameter.get("songName"));
		playlist.setArtistcategoryCode(Integer.parseInt(parameter.get("artistcategoryCode")));
		
		if(playlistService.modifyPlaylist(playlist)) {
			printResult.printSuccessMessage("update");
		} else {
			printResult.printErrorMessage("update");
		}
		
		
	}

	public void deletePlaylist(Map<String, String> parameter) {
		
		String song = parameter.get("song");
		
		
		if(playlistService.deletePlaylist(song)) {
			printResult.printSuccessMessage("delete");
		} else {
			printResult.printErrorMessage("delete");
		}
		
	}

	




	
	
}
