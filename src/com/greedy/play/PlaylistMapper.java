package com.greedy.play;

import java.util.List;
import java.util.Map;

import com.greedy.common.PlaylistDTO;

public interface PlaylistMapper {
	
	List<PlaylistDTO> selectAllPlaylist();

	List<PlaylistDTO> selectPlaylistByArtist(String name);
	
	List<PlaylistDTO> selectRandomPlaylist(Map<String, List<Integer>> criteria);

	int insertPlaylist(PlaylistDTO playlist);

	int updatePlaylist(PlaylistDTO playlist);

	int deletePlaylist(String song);



}
