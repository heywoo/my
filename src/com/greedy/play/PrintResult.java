package com.greedy.play;

import java.util.List;

import com.greedy.common.PlaylistDTO;

public class PrintResult {

	
	public void printPlist(List<PlaylistDTO> plist) {
		for(PlaylistDTO playlist : plist) {
			System.out.println(playlist);
		}
	}		
		
	public void printPlaylist(List<PlaylistDTO> playlist) {
		for(PlaylistDTO serchplaylist : playlist)
		System.out.println(serchplaylist);
			
	}
	

	public void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList" : errorMessage = "플레이리스트 전제 재생에 실패했습니다."; break;
		case "selectname" : errorMessage = "아티스트 검색에 실패했습니다."; break;
		case "selectRandom" : errorMessage = "랜덤플레이리스트 재생에 실패했습니다."; break;
		case "insert":  errorMessage = "플레이리스트 추가에 실패했습니다."; break;
		case "update" : errorMessage = "플레이리스트 수정에 실패하였습니다."; break;
		case "delete" : errorMessage = "플레이리스트 삭제에 실패하였습니다."; break;
		}
		
		System.out.println(errorMessage);
	}

	
	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "플레이리스트 추가에 성공했습니다."; break;
		case "update" : successMessage = "플레이리스트 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "플레이리스트 삭제에 성공하였습니다."; break;
		}
		
		System.out.println(successMessage);
	}

	

}
