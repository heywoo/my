package com.greedy.play;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.greedy.common.PlaylistDTO;
import com.greedy.common.SearchCriteria;

import static com.greedy.common.Template.getSqlSession;

public class PlaylistService {
	
	private PlaylistMapper playlistMapper;

	public List<PlaylistDTO> selectAllPlaylist() {
		
		SqlSession sqlSession = getSqlSession();
		playlistMapper = sqlSession.getMapper(PlaylistMapper.class);
		List<PlaylistDTO> plist = playlistMapper.selectAllPlaylist();
		
		sqlSession.close();
		
		return plist;
	
	}

	public List<PlaylistDTO> selectPlaylistByArtist(String name) {
		
		SqlSession sqlSession = getSqlSession();
		playlistMapper = sqlSession.getMapper(PlaylistMapper.class);
		
		List<PlaylistDTO> playlist = playlistMapper.selectPlaylistByArtist(name);
	
		sqlSession.close();
		
		return playlist;
	}
	
	public void selectRandomPlaylist(List<Integer> random) {
		
		SqlSession sqlSession = getSqlSession();
		playlistMapper = sqlSession.getMapper(PlaylistMapper.class);
		
		Map<String, List<Integer>> criteria = new HashMap<>();
		criteria.put("random", random);
		
		List<PlaylistDTO> plist = playlistMapper.selectRandomPlaylist(criteria);
		
		if(plist != null && !plist.isEmpty()) {
			for(PlaylistDTO playlistDTO : plist) {
				System.out.println(playlistDTO);
			}
		} else {
			System.out.println("랜럼 플레이에 실패했습니다.");
		}
	
		sqlSession.close();
		

	}

	public boolean registPlaylist(PlaylistDTO playlist) {
		SqlSession sqlSession = getSqlSession();
		playlistMapper = sqlSession.getMapper(PlaylistMapper.class);
		
		int result = playlistMapper.insertPlaylist(playlist);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean modifyPlaylist(PlaylistDTO playlist) {
		SqlSession sqlSession = getSqlSession();
		playlistMapper = sqlSession.getMapper(PlaylistMapper.class);
		
		int result = playlistMapper.updatePlaylist(playlist);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
		
		
	}

	public boolean deletePlaylist(String song) {
		
		SqlSession sqlSession = getSqlSession();
		playlistMapper = sqlSession.getMapper(PlaylistMapper.class);
		
		int result = playlistMapper.deletePlaylist(song);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}


	
}
