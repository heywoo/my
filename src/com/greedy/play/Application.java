package com.greedy.play;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Application {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		PlaylistController playlistController = new PlaylistController();
		
		    
		do {
			System.out.println();
			System.out.println("  𝑴𝒀 𝑷𝑳𝑨𝒀𝑳𝑰𝑺𝑻  ღ(ᖰ˘◡˘ᖳ)ღ•*¨*•.¸¸♪  ");
			System.out.println("𝟙. 플레이리스트 전체 재생");
			System.out.println("𝟚. 아티스트 이름으로 플레이리스트 검색");
			System.out.println("𝟛. 플레이리스트 5곡 랜덤 재생");
			System.out.println("𝟜. 새로운 노래 추가");
			System.out.println("𝟝. 플레이리스트 노래 수정");
			System.out.println("𝟞. 플레이리스트 노래 삭제");
			System.out.print("  𝑺𝑬𝑳𝑬𝑪𝑻 𝑵𝑼𝑴𝑩𝑬𝑹 : ");
			
			int no = sc.nextInt();
			
			switch(no) {
			case 1: playlistController.selectAllPlaylist(); break;
			case 2: playlistController.selectPlaylistByArtist(inputArtist()); break;
			case 3: playlistController.randomplay(); break;
			case 4: playlistController.insertPlaylist(inputPlaylist()); break;
			case 5: playlistController.modifyPlaylist(inputModifyPlaylist());break;
			case 6: playlistController.deletePlaylist(inputSong()); break;
			default: System.out.println("  𝑾𝑹𝑶𝑵𝑮 𝑵𝑼𝑴𝑩𝑬𝑹  ");
			
			}
			
		} while (true);

	} 


	private static Map<String, String> inputArtist() {
		Scanner sc = new Scanner(System.in);
		System.out.print("  𝑨𝑹𝑻𝑰𝑺𝑻 𝑵𝑨𝑴𝑬 𝑰𝑺 : ");
		String name = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("name", name);
				
		return parameter;
	}


	private static Map<String, String> inputPlaylist() {
		Scanner sc = new Scanner(System.in);
		System.out.print("아티스트 이름 : ");
		String artistName = sc.nextLine();
		System.out.print("노래 제목 : ");
		String songName = sc.nextLine();
		System.out.print("아티스트 코드 : ");
		String artistcategoryCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("artistName", artistName);
		parameter.put("songName", songName);
		parameter.put("artistcategoryCode", artistcategoryCode);
		
		return parameter;
	}
	
	private static Map<String, String> inputModifyPlaylist() {
		Scanner sc = new Scanner(System.in);
		System.out.print("수정할 플레이리스트 순서 : ");
		String order = sc.nextLine();
		System.out.print("수정할 아티스트 이름 : ");
		String artistName = sc.nextLine();
		System.out.print("수정할 노래 제목: ");
		String songName = sc.nextLine();
		System.out.print("수정할 아티스트 코드 : ");
		String artistcategoryCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("order", order);
		parameter.put("artistName", artistName);
		parameter.put("songName", songName);
		parameter.put("artistcategoryCode", artistcategoryCode);
		
		return parameter;
	}
	
	private static Map<String, String> inputSong() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("삭제할 노래 제목 : ");
		String song = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("song", song);
		
		return parameter;
	}
}
