package com.greedy.common;

public class PlaylistDTO {

	private int playOrder;
	private String artistName;
	private String songName;
	private int artistcategoryCode;
	private String likeStatus;
	
	
	public PlaylistDTO () {}


	public PlaylistDTO(int playOrder, String artistName, String songName, int artistcategoryCode, String likeStatus) {
		super();
		this.playOrder = playOrder;
		this.artistName = artistName;
		this.songName = songName;
		this.artistcategoryCode = artistcategoryCode;
		this.likeStatus = likeStatus;
	}


	public int getPlayOrder() {
		return playOrder;
	}


	public void setPlayOrder(int playOrder) {
		this.playOrder = playOrder;
	}


	public String getArtistName() {
		return artistName;
	}


	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}


	public String getSongName() {
		return songName;
	}


	public void setSongName(String songName) {
		this.songName = songName;
	}


	public int getArtistcategoryCode() {
		return artistcategoryCode;
	}


	public void setArtistcategoryCode(int artistcategoryCode) {
		this.artistcategoryCode = artistcategoryCode;
	}


	public String getLikeStatus() {
		return likeStatus;
	}


	public void setLikeStatus(String likeStatus) {
		this.likeStatus = likeStatus;
	}


	@Override
	public String toString() {
		return "PlaylistDTO [playOrder=" + playOrder + ", artistName=" + artistName + ", songName=" + songName
				+ ", artistcategoryCode=" + artistcategoryCode + ", likeStatus=" + likeStatus + "]";
	}
	
	
	

	
}
